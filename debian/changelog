behave (1.2.6-6) unstable; urgency=medium

  * Team upload.
  * Remove remaining usage of Nose (Closes: #1018314)
  * Swap Maintainer <-> Uploaders per new DPT Policy
  * Use dh-sequence-python3
  * Apply the Multi-Arch hint
  * Set Rules-Requires-Root: no

 -- Alexandre Detiste <tchet@debian.org>  Sat, 14 Sep 2024 16:08:58 +0200

behave (1.2.6-5) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

  [ Christoph Berg ]
  * Fix FTBFS with Sphinx 7.1 and extlinks. (Closes: #1042610)
  * Move "tests" directory aside while running test. (Closes: #1054974)

 -- Christoph Berg <myon@debian.org>  Mon, 15 Jan 2024 15:43:14 +0000

behave (1.2.6-4) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Christoph Berg ]
  * setup.py: Remove use_2to3 attribute. (Closes: #997632)

 -- Christoph Berg <myon@debian.org>  Thu, 09 Dec 2021 15:32:01 +0100

behave (1.2.6-3) unstable; urgency=medium

  * Team upload.
  * debian/patches: Add 0004_FTBFS_with_pytest.patch, fix FTBFS,
    thanks to  jenisys. (Closes: #977057).


 -- Joao Paulo Lima de Oliveira <jlima.oliveira11@gmail.com>  Thu, 18 Feb 2021 18:51:33 -0400

behave (1.2.6-2) unstable; urgency=medium

  * Team upload.

  [ Michael Banck ]
  * d/control: Added python3-path and python3-pytest to Build-Depends, thanks
    to Sebastiaan Couwenberg. Closes: #954645.

  [ Christoph Berg ]
  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <myon@debian.org>  Fri, 08 May 2020 20:47:54 +0200

behave (1.2.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream relesae.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Michael Banck ]
  * debian/patches/0001-docs-disable-use-of-sphincontrib.cheeseshop.patch:
    Removed, no longer needed.
  * debian/patches/0002-Python-3.6-compatibility-patch-re.LOCALE-removed.patch:
    Removed, no longer needed.
  * debian/patches/0003-Backport-for-py38-fixes.patch: New patch, provides
    compatibility with Python 3.8. Closes: #943991.
  * debian/control: Added python3-sphinx-bootstrap-theme to Build-Depends.

 -- Christoph Berg <myon@debian.org>  Sat, 25 Jan 2020 13:59:42 +0100

behave (1.2.5-3) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support (Closes: #936200).
  * Enable autopkgtest-pkg-python testsuite.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0.

 -- Ondřej Nový <onovy@debian.org>  Fri, 13 Sep 2019 15:19:10 +0200

behave (1.2.5-2) unstable; urgency=medium

  * debian/patches/0002-Python-3.6-compatibility.patch: Apply patch from
    upstream to provide compatibility with Python 3.6 and fix
    FTBFS. Thanks to Logan Rosen for the patch. Closes: #870586.

 -- Vincent Bernat <bernat@debian.org>  Sat, 05 Aug 2017 12:40:02 +0200

behave (1.2.5-1) unstable; urgency=low

  * Initial release (closes: #765550)

 -- Vincent Bernat <bernat@debian.org>  Tue, 13 Jun 2017 07:01:28 +0200
